/*
 * File:   pcf2123.h
 * Author: Miroslav Soukup
 * Description: Header file of pcf2123 RTC module driver.
 * 
 */



#ifndef PCF2123_H // Protection against multiple inclusion
#define PCF2123_H



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types 



#ifdef __cplusplus  // Provide C++ compatibility
extern "C" {
#endif



// *****************************************************************************
// Section: Macros
// *****************************************************************************

#define PCF2123_REG_CONTROL_1                       UINT8_C(0x00)
#define PCF2123_REG_CONTROL_2                       UINT8_C(0x01)
#define PCF2123_REG_SECONDS                         UINT8_C(0x02)
#define PCF2123_REG_MINUTES                         UINT8_C(0x03)
#define PCF2123_REG_HOURS                           UINT8_C(0x04)
#define PCF2123_REG_DAYS                            UINT8_C(0x05)
#define PCF2123_REG_WEEKDAYS                        UINT8_C(0x06)
#define PCF2123_REG_MONTHS                          UINT8_C(0x07)
#define PCF2123_REG_YEARS                           UINT8_C(0x08)
#define PCF2123_REG_MINUTE_ALARM                    UINT8_C(0x09)
#define PCF2123_REG_HOUR_ALARM                      UINT8_C(0x0A)
#define PCF2123_REG_DAY_ALARM                       UINT8_C(0x0B)
#define PCF2123_REG_WEEKDAY_ALARM                   UINT8_C(0x0C)
#define PCF2123_REG_OFFSET                          UINT8_C(0x0D)
#define PCF2123_REG_TIMER_CLKOUT                    UINT8_C(0x0E)
#define PCF2123_REG_COUNTDOWN_TIMER                 UINT8_C(0x0F)
    


// *****************************************************************************
// Section: Data types
// *****************************************************************************

typedef struct pcf2123_descriptor pcf2123_t;
typedef struct pcf2123_data_descriptor pcf2123_data_t;
typedef struct pcf2123_data_time_descriptor pcf2123_data_time_t;
typedef struct pcf2123_data_date_descriptor pcf2123_data_date_t;
typedef struct pcf2123_config_descriptor pcf2123_config_t;
typedef struct pcf2123_config_datetime_descriptor pcf2123_config_datetime_t;

typedef uint8_t (*pcf2123_spi_rw_funcptr_t)(uint8_t *data, uint8_t size);
typedef void (*pcf2123_spi_ce_funcptr_t)(void);


typedef enum{
    PCF2123_CONFIG_1_EXT_TEST_NORMAL_MODE           = 0b0, // power-up default
    PCF2123_CONFIG_1_EXT_TEST_EXTERNAL_CLOCK_SOURCE = 0b1
} PCF2123_CONFIG_1_EXT_TEST_e;

typedef enum{
    PCF2123_CONFIG_1_STOP_CLOCK_RUNS = 0b0, // power-up default
    PCF2123_CONFIG_1_STOP_CLOCK_STOP = 0b1
} PCF2123_CONFIG_1_STOP_e;

typedef enum{
    PCF2123_CONFIG_1_SR_NO_SOFTWARE_RESET       = 0b0, // power-up default
    PCF2123_CONFIG_1_SR_INITIATE_SOFTWARE_RESET = 0b1
} PCF2123_CONFIG_1_SR_e;

typedef enum{
    PCF2123_CONFIG_1_1224_MODE_24 = 0b0, // power-up default
    PCF2123_CONFIG_1_1224_MODE_12 = 0b1
} PCF2123_CONFIG_1_1224_e;

typedef enum{
    PCF2123_CONFIG_1_CIE_NO_CORRECTION_INT       = 0b0, // power-up default
    PCF2123_CONFIG_1_CIE_GENERATE_CORRECTION_INT = 0b1
} PCF2123_CONFIG_1_CIE_e;

typedef enum{
    PCF2123_CONFIG_2_MI_MINUTE_INT_DISABLED = 0b0, // power-up default
    PCF2123_CONFIG_2_MI_MINUTE_INT_ENABLED  = 0b1
} PCF2123_CONFIG_2_MI_e;

typedef enum{
    PCF2123_CONFIG_2_SI_SECOND_INT_DISABLED = 0b0, // power-up default
    PCF2123_CONFIG_2_SI_SECOND_INT_ENABLED  = 0b1
} PCF2123_CONFIG_2_SI_e;

typedef enum{
    PCF2123_CONFIG_2_MSF_MINUTE_SECOND_INT_NOT_GENERATED = 0b0, // power-up default
    PCF2123_CONFIG_2_MSF_MINUTE_SECOND_INT_GENERATED  = 0b1
} PCF2123_CONFIG_2_MSF_e;

typedef enum{
    PCF2123_CONFIG_2_TI_TP_PIN_FOLLOWS_TIMER_FLAGS = 0b0, // power-up default
    PCF2123_CONFIG_2_TI_TP_GENERATE_PULSE_ON_PIN   = 0b1
} PCF2123_CONFIG_2_TI_TP_e;

typedef enum{
    PCF2123_CONFIG_2_AF_ALARM_INT_NOT_GENERATED = 0b0, // power-up default
    PCF2123_CONFIG_2_AF_ALARM_INT_GENERATED     = 0b1
} PCF2123_CONFIG_2_AF_e;

typedef enum{
    PCF2123_CONFIG_2_TF_COUNTDOWN_INT_NOT_GENERATED = 0b0, // power-up default
    PCF2123_CONFIG_2_TF_COUNTDOWN_INT_GENERATED     = 0b1
} PCF2123_CONFIG_2_TF_e;

typedef enum{
    PCF2123_CONFIG_2_AIE_ALARM_FLAG_INT_DISABLED = 0b0, // power-up default
    PCF2123_CONFIG_2_AIE_ALARM_FLAG_INT_ENABLED  = 0b1
} PCF2123_CONFIG_2_AIE_e;

typedef enum{
    PCF2123_CONFIG_2_TIE_COUNTDOWN_TIMER_INT_DISABLED = 0b0, // power-up default
    PCF2123_CONFIG_2_TIE_COUNTDOWN_TIMER_INT_ENABLEd  = 0b1
} PCF2123_CONFIG_2_TIE_e;



struct pcf2123_config_datetime_descriptor{
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
    uint8_t day;
    uint8_t month;
    uint16_t year;
};

struct pcf2123_config_descriptor{
    uint8_t hw_cs_enabled;
    
    pcf2123_config_datetime_t datetime;
    
    PCF2123_CONFIG_1_EXT_TEST_e ext_test;
    PCF2123_CONFIG_1_STOP_e stop;
    PCF2123_CONFIG_1_SR_e sr;
    PCF2123_CONFIG_1_1224_e time_format;
    PCF2123_CONFIG_1_CIE_e cie;
    
    PCF2123_CONFIG_2_MI_e mi;
    PCF2123_CONFIG_2_SI_e si;
    PCF2123_CONFIG_2_MSF_e msf;
    PCF2123_CONFIG_2_TI_TP_e ti_tp;
    PCF2123_CONFIG_2_AF_e af;
    PCF2123_CONFIG_2_TF_e tf;
    PCF2123_CONFIG_2_AIE_e aie;
    PCF2123_CONFIG_2_TIE_e tie;
};

struct pcf2123_data_time_descriptor{
    uint8_t hour;
    uint8_t minute;
    uint8_t second;
};

struct pcf2123_data_date_descriptor{
    uint8_t day;
    uint8_t weekday;
    uint8_t month;
    uint16_t year;
};

struct pcf2123_data_descriptor{
    pcf2123_data_time_t time;
    pcf2123_data_date_t date;
};

struct pcf2123_descriptor{
    pcf2123_config_t config;
    pcf2123_spi_rw_funcptr_t spi_write;
    pcf2123_spi_rw_funcptr_t spi_read;
    pcf2123_spi_ce_funcptr_t spi_ce;
    pcf2123_spi_ce_funcptr_t spi_cd;
    pcf2123_data_t data;
};



// *****************************************************************************
// Section: Function prototypes
// *****************************************************************************

/**
 * \brief Function for initialization pcf2123 module.
 *
 * \param me pointer to pcf2123 sensor type of pcf2123_t
 * \param config pointer to pcf2123 configuration type of pcf2123_config_t
 *
 * \returns status of success / failure
 */
uint8_t pcf2123_init (pcf2123_t *me, pcf2123_config_t *config, pcf2123_config_datetime_t *datetime);

uint8_t pcf2123_spi_write_register (pcf2123_t *me, pcf2123_spi_rw_funcptr_t spi_write_funcptr);

uint8_t pcf2123_spi_read_register (pcf2123_t *me, pcf2123_spi_rw_funcptr_t spi_read_funcptr);

uint8_t pcf2123_spi_chip_enable_register (pcf2123_t *me, pcf2123_spi_ce_funcptr_t spi_ce_funcptr);

uint8_t pcf2123_spi_chip_disable_register (pcf2123_t *me, pcf2123_spi_ce_funcptr_t spi_cd_funcptr);

uint8_t pcf2123_get_datetime (pcf2123_t *me);

uint8_t pcf2123_get_date (pcf2123_t *me);

uint8_t pcf2123_get_time (pcf2123_t *me);

uint8_t pcf2123_set_time (pcf2123_t *me, uint8_t hours, uint8_t minutes, uint8_t seconds);

uint8_t pcf2123_set_date (pcf2123_t *me, uint8_t days, uint8_t months, uint16_t years);

uint8_t get_weekday_from_date (uint8_t day, uint8_t month, uint16_t year);



#ifdef __cplusplus // End of C++ compatibility
}
#endif

    
#endif // End of PCF2123_H
