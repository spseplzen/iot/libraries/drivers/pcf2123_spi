/*
 * File:   pcf2123.h
 * Author: Miroslav Soukup
 * Description: Source file of pcf2123 RTC module driver.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "pcf2123.h"


// *****************************************************************************
// Section: Functions
// *****************************************************************************

static uint8_t _pcf2123_spi_read_reg(pcf2123_t *me, uint8_t reg, uint8_t *byte){
    if(me->config.hw_cs_enabled) me->spi_ce();
    reg = 0b10010000 | reg;
    if(!me->spi_write(&reg, 1)) return 0;
    if(!me->spi_read(byte, 1)) return 0;
    if(me->config.hw_cs_enabled) me->spi_cd();
    return 1;
}

static uint8_t _pcf2123_spi_write_reg(pcf2123_t *me, uint8_t reg, uint8_t byte){
    uint8_t _data[2];
    _data[0] = 0b00010000 | reg;
    _data[1] = byte;
    if(me->config.hw_cs_enabled) me->spi_ce();
    if(!me->spi_write(_data, 2)) return 0;
    if(me->config.hw_cs_enabled) me->spi_cd();
    return 1;
}

uint8_t pcf2123_init (pcf2123_t *me, pcf2123_config_t *config, pcf2123_config_datetime_t *datetime) {
    
    me->config = *config;
    
    /* ************************ CONTROL 1 ************************ */
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_CONTROL_1, me->config.ext_test << 7 | me->config.stop << 5 | me->config.sr << 4 | me->config.time_format << 2 | me->config.cie << 1)) return 0;
    /* *********************************************************** */
    
    /* ************************ CONTROL 2 ************************ */
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_CONTROL_2, me->config.mi << 7 | me->config.si << 6 | me->config.msf << 5 | me->config.ti_tp << 4 | me->config.af << 3 | me->config.tf << 2 | me->config.aie << 1 | me->config.tie)) return 0;
    /* *********************************************************** */
    
    /* ************************* SECONDS ************************* */
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_SECONDS, (datetime->second / 10) << 4 | (datetime->second % 10))) return 0;
    /* *********************************************************** */
    
    /* ************************* MINUTES ************************* */
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_MINUTES, (datetime->minute / 10) << 4 | (datetime->minute % 10))) return 0;
    /* *********************************************************** */
    
    /* ************************** HOURS ************************** */
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_HOURS, (datetime->hour / 10) << 4 | (datetime->hour % 10))) return 0;
    /* *********************************************************** */
    
    /* ************************** DAYS *************************** */
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_DAYS, (datetime->day / 10) << 4 | (datetime->day % 10))) return 0;
    /* *********************************************************** */
    
    /* ************************ WEEKDAYS ************************* */
    uint8_t weekday = get_weekday_from_date(datetime->day, datetime->month, datetime->year);
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_WEEKDAYS, (weekday / 10) << 4 | (weekday % 10))) return 0;
    /* *********************************************************** */
    
    /* ************************* MONTHS ************************** */
    datetime->month = datetime->month - 1;
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_MONTHS, (datetime->month / 10) << 4 | (datetime->month % 10))) return 0;
    /* *********************************************************** */
    
    /* ************************** YEARS ************************** */
    datetime->year = datetime->year - 2000;
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_YEARS, (datetime->year / 10) << 4 | (datetime->year % 10))) return 0;
    /* *********************************************************** */
    
    return 1;
}

uint8_t pcf2123_spi_write_register (pcf2123_t *me, pcf2123_spi_rw_funcptr_t spi_write_funcptr){
    if(!spi_write_funcptr) return 0;
    me->spi_write = spi_write_funcptr;
    return 1;
}

uint8_t pcf2123_spi_read_register (pcf2123_t *me, pcf2123_spi_rw_funcptr_t spi_read_funcptr){
    if(!spi_read_funcptr) return 0;
    me->spi_read = spi_read_funcptr;
    return 1;
}

uint8_t pcf2123_spi_chip_enable_register (pcf2123_t *me, pcf2123_spi_ce_funcptr_t spi_ce_funcptr){
    if(!spi_ce_funcptr) return 0;
    me->spi_ce = spi_ce_funcptr;
    return 1;
}

uint8_t pcf2123_spi_chip_disable_register (pcf2123_t *me, pcf2123_spi_ce_funcptr_t spi_cd_funcptr){
    if(!spi_cd_funcptr) return 0;
    me->spi_cd = spi_cd_funcptr;
    return 1;
}

uint8_t pcf2123_get_datetime (pcf2123_t *me){
    uint8_t _data;
    
    me->data.time.second = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_SECONDS, &_data)) return 0;
    me->data.time.second = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF));
    
    me->data.time.minute = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_MINUTES, &_data)) return 0;
    me->data.time.minute = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF));
    
    me->data.time.hour = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_HOURS, &_data)) return 0;
    me->data.time.hour = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF));
    
    me->data.date.day = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_DAYS, &_data)) return 0;
    me->data.date.day = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF));
    
    me->data.date.weekday = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_WEEKDAYS, &_data)) return 0;
    me->data.date.weekday = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF));
    
    me->data.date.month = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_MONTHS, &_data)) return 0;
    me->data.date.month = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF)) + 1;
    
    me->data.date.year = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_YEARS, &_data)) return 0;
    me->data.date.year = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF)) + 2000;
    
    return 1;
}

uint8_t pcf2123_get_date (pcf2123_t *me){
    uint8_t _data;
    
    me->data.date.day = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_DAYS, &_data)) return 0;
    me->data.date.day = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF));
    
    me->data.date.weekday = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_WEEKDAYS, &_data)) return 0;
    me->data.date.weekday = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF));
    
    me->data.date.month = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_MONTHS, &_data)) return 0;
    me->data.date.month = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF)) + 1;
    
    me->data.date.year = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_YEARS, &_data)) return 0;
    me->data.date.year = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF)) + 2000;
    
    return 1;
}

uint8_t pcf2123_get_time (pcf2123_t *me){
    uint8_t _data;
    
    me->data.time.second = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_SECONDS, &_data)) return 0;
    me->data.time.second = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF));
    
    me->data.time.minute = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_MINUTES, &_data)) return 0;
    me->data.time.minute = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF));
    
    me->data.time.hour = 0;
    if(!_pcf2123_spi_read_reg(me, PCF2123_REG_HOURS, &_data)) return 0;
    me->data.time.hour = ((((_data >> 4) & 0xF) * 10) + (_data & 0xF));
    
    return 1;
}

uint8_t pcf2123_set_time (pcf2123_t *me, uint8_t hours, uint8_t minutes, uint8_t seconds){
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_SECONDS, (seconds / 10) << 4 | (seconds % 10))) return 0;
    
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_MINUTES, (minutes / 10) << 4 | (minutes % 10))) return 0;
    
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_HOURS, (hours / 10) << 4 | (hours % 10))) return 0;
    
    return 1;
}

uint8_t pcf2123_set_date (pcf2123_t *me, uint8_t days, uint8_t months, uint16_t years){
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_DAYS, (days / 10) << 4 | (days % 10))) return 0;

    uint8_t weekday = get_weekday_from_date(days, months, years);
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_WEEKDAYS, (weekday / 10) << 4 | (weekday % 10))) return 0;

    months = months - 1;
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_MONTHS, (months / 10) << 4 | (months % 10))) return 0;

    years = years - 2000;
    if(!_pcf2123_spi_write_reg(me, PCF2123_REG_YEARS, (years / 10) << 4 | (years % 10))) return 0;
    
    return 1;
}

uint8_t get_weekday_from_date (uint8_t day, uint8_t month, uint16_t year){
    if (month < 3) {
        month += 12;
        year -= 1;
    }

    uint16_t K = year % 100;
    uint16_t J = year / 100;

    // Zelleruv algoritmus vraci hodnoty 0 (sobota) az 6 (patek)
    uint8_t day_of_week = (day + ((13 * (month + 1)) / 5) + K + ((K / 4) + (J / 4)) - (2 * J)) % 7;
    
    // prevedeme na nase potreby 0 (nedele) az 6 (sobota)
    return (day_of_week + 1) % 7;
}
