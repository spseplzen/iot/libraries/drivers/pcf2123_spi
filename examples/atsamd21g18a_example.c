#include <stddef.h>                     // Defines NULL
#include <stdbool.h>                    // Defines true
#include <stdlib.h>                     // Defines EXIT_FAILURE
#include "definitions.h"                // SYS function prototypes

#include "pcf2123/pcf2123.h"


volatile uint8_t sw0_flag = 0;
void sw0_callback(uintptr_t context){
    sw0_flag = 1;
}

volatile uint8_t sw1_flag = 0;
void sw1_callback(uintptr_t context){
    sw1_flag = 1;
}

volatile uint8_t int_flag = 0;
void int_callback(uintptr_t context){
    int_flag = 1;
}

uint8_t spi_write(uint8_t *data, uint8_t size){
    SERCOM0_SPI_Write(data, size);
    while(SERCOM0_SPI_IsBusy());
    return 1;
}

uint8_t spi_read(uint8_t *data, uint8_t size){
    SERCOM0_SPI_Read(data, size);
    while(SERCOM0_SPI_IsBusy());
    return 1;
}

void spi_chip_enable(void){
    mikroBUS_SPI_CS_Set();
}

void spi_chip_disable(void){
    mikroBUS_SPI_CS_Clear();
}

int main ( void )
{
    SYS_Initialize ( NULL );
    
    EIC_CallbackRegister(EIC_PIN_0, sw0_callback, (uintptr_t) NULL);
    EIC_CallbackRegister(EIC_PIN_1, sw1_callback, (uintptr_t) NULL);
    EIC_CallbackRegister(EIC_PIN_12, int_callback, (uintptr_t) NULL);
    
    
    pcf2123_t pcf2123;
    
    pcf2123_spi_write_register(&pcf2123, spi_write);
    pcf2123_spi_read_register(&pcf2123, spi_read);
    pcf2123_spi_chip_enable_register(&pcf2123, spi_chip_enable);
    pcf2123_spi_chip_disable_register(&pcf2123, spi_chip_disable);
    
    
    pcf2123_config_t config;
    config.hw_cs_enabled = 1;
    
    config.ext_test    = PCF2123_CONFIG_1_EXT_TEST_NORMAL_MODE;
    config.stop        = PCF2123_CONFIG_1_STOP_CLOCK_RUNS;
    config.sr          = PCF2123_CONFIG_1_SR_NO_SOFTWARE_RESET;
    config.time_format = PCF2123_CONFIG_1_1224_MODE_24;
    config.cie         = PCF2123_CONFIG_1_CIE_NO_CORRECTION_INT;
    
    config.mi          = PCF2123_CONFIG_2_MI_MINUTE_INT_DISABLED;
    config.si          = PCF2123_CONFIG_2_SI_SECOND_INT_ENABLED;
    config.msf         = PCF2123_CONFIG_2_MSF_MINUTE_SECOND_INT_NOT_GENERATED;
    config.ti_tp       = PCF2123_CONFIG_2_TI_TP_GENERATE_PULSE_ON_PIN;
    config.af          = PCF2123_CONFIG_2_AF_ALARM_INT_NOT_GENERATED;
    config.tf          = PCF2123_CONFIG_2_TF_COUNTDOWN_INT_NOT_GENERATED;
    config.aie         = PCF2123_CONFIG_2_AIE_ALARM_FLAG_INT_DISABLED;
    config.tie         = PCF2123_CONFIG_2_TIE_COUNTDOWN_TIMER_INT_DISABLED;
    
    
    pcf2123_config_datetime_t datetime;
    datetime.day    = 8;
    datetime.month  = 12;
    datetime.year   = 2023;
    datetime.hour   = 20;
    datetime.minute = 10;
    datetime.second = 30;
    
    pcf2123_init(&pcf2123, &config, &datetime);
    
    
    printf("Testovani driveru pro RTC 13 Click\r\n");

    while ( true )
    {
        SYS_Tasks ( );
        
        if(int_flag){
            int_flag = 0;
            
            pcf2123_get_datetime(&pcf2123);
            
            printf("Datum a cas je: %02u:%02u:%02u %02u.%02u. %04u\r\n", pcf2123.data.time.hour, pcf2123.data.time.minute, pcf2123.data.time.second, pcf2123.data.date.day, pcf2123.data.date.month, pcf2123.data.date.year);
            
        }
        
    }

    return ( EXIT_FAILURE );
}
